import os
from setuptools import setup, find_packages


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="oc-rsh",
    version="0.0.3",
    author="Drew Stinnett",
    author_email="drew.stinnett@duke.edu",
    description=("Prompt for rsh container using fzf"),
    license="BSD",
    keywords="oc openshift okd",
    packages=find_packages(),
    scripts=['scripts/oc-rsh'],
    long_description=read('README.md'),
)
