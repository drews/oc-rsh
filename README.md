# oc-rsh

Helper script to bring up all available rsh terminals in [fzf](https://github.com/junegunn/fzf)

By default only looks in current namespace, use `-a` for all namespaces

To use escalated privilages, pass `-s` or `--sudo` in as an argument

## Installation

From Source:

```
python3 ./setup.py install
```

Using Dukes PyPi

```
export PIP_EXTRA_INDEX_URL=https://piepie.oit.duke.edu/simple/
pip3 install oc-rsh
```

## Example Usage

![tty](media/tty.gif)
